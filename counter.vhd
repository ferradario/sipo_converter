-------------------------------------------------------------------------------
-- Title       : counter
-- Design      : sipo_converter
-- Author      : Dario Ferrarotti
-------------------------------------------------------------------------------
--
-- Description : COUNTER: This module takes as input the variable first_bit that 
--  signals the start of a new series of 8 bits. From there it has to count eight
--  clock cycle and to raise the value of the output variable last_bit during the 
--  clock cycle in which eight is reached.
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all; 
use ieee.std_logic_unsigned.ALL;  -- library to allow operation between logic vectors

entity counter is
	 port(
		 clock 			: in  STD_LOGIC;
		 first_bit 		: in  STD_LOGIC;
		 last_bit 	    : out STD_LOGIC := '0'
	     );
end counter;


architecture BEHAVIOURAL of counter is	 

signal count : std_logic_vector (2 downto 0);

begin
	
	process ( clock, first_bit) 
	
	variable counting : BOOLEAN := FALSE;

	begin 
	
	-- Asynchronous reset. To be sensible whenever the sequence arrives
	if (first_bit='1') then
		count <= "000";
		counting := TRUE;
	end if;
	
	-- At each clock cycle
	if (clock 'EVENT and clock = '1') then
		
		-- increment the value for count
		count <= count +1;
		
		-- at 8th clock cycle put to one last_bit, then put it to zero 
		-- at the next clock
		if (count = 0) then 
			last_bit <= '0';
		end IF;
	
		if (count = 7) and (counting) then
			last_bit <= '1';
		end if;
		
	end if;				
		
	end process;
	
end BEHAVIOURAL;