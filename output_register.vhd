-------------------------------------------------------------------------------
-- Title       : output_register
-- Design      : sipo_converter
-- Author      : Dario Ferrarotti
-------------------------------------------------------------------------------
-- Description : OUTPUT MODULE: This module is responsible of outputting the parallelized 
--	data and the signal data_out_valid. 
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity output_register is
	 port(
	 		clock			: in  STD_LOGIC;			
	 		last_bit		: in  STD_LOGIC;			
		 	data_in			: in  STD_LOGIC_VECTOR(7 downto 0);
		 	data_out 		: out STD_LOGIC_VECTOR(7 downto 0);
			data_out_valid	: out STD_LOGIC
	     );
end output_register;

architecture BEHAVIOURAL of output_register is
begin
	process(clock)
	begin
		-- at each clock cycle
		if(clock = '1' and clock 'EVENT) then
			-- if input last_bit is high
			if(last_bit = '1') then		
				-- data_in is transfert to the output
				data_out <= data_in;
			end if;
			-- always last_bit is transfert to the output
			data_out_valid <= last_bit;
			
		end if;
	end process;
end BEHAVIOURAL;
