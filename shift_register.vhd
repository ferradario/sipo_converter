-------------------------------------------------------------------------------
-- Title       : shift_register
-- Design      : sipo_converter
-- Author      : Dario Ferrarotti
-------------------------------------------------------------------------------
-- File        : c:\My_Designs\sipo_converter\sipo_converter\src\shift_register.vhd
-- Generated   : Sat Jan  4 21:36:29 2014
-- From        : interface description file
-- By          : Itf2Vhdl ver. 1.22
-------------------------------------------------------------------------------
-- Description : A simple 8-bit shift register. At each clock cycle the data is shifted
--				to the left and the data in input is stored in the rightmost flip flop
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity shift_register is
	 port(
		 	 clock 		: in STD_LOGIC;
		 	 data_in 	: in STD_LOGIC;
			 d			: inout STD_LOGIC_VECTOR(7 downto 0) -- inout to allow the shifting
	     );
end shift_register;


architecture BEHAVIOURAL of shift_register is
begin
	process(clock)
	begin
		-- at each clock cycle
		if(clock='1' and clock 'EVENT) then
			-- copy one position to the right ( d7-d1 in d6-d0)
			d(6 downto 0) <= d(7 downto 1);
			-- put the data in input in d7  (the old value of d7 is now in d6) 
			d(7) <= data_in;
		end if;
	end process;

end BEHAVIOURAL;