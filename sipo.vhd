-------------------------------------------------------------------------------
-- Title       : sipo
-- Design      : sipo_converter
-- Author      : Dario Ferrarotti
-------------------------------------------------------------------------------
-- Description : This is the main entity responsible to map the three components:
--  	counter, shift_register, output_register
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity sipo is
	 port(
			 clock 			: in  STD_LOGIC;
		 	 first_bit 		: in  STD_LOGIC;
			 data_in 		: in  STD_LOGIC;
			 data_out 		: out std_logic_VECTOR (7 downto 0);
			 data_out_valid : out STD_LOGIC
	     );
end sipo;

architecture sipo of sipo is  

-- Import components: counter, shift_register, output_register
component	counter
	port(
		 	clock 			: in  STD_LOGIC;
		 	first_bit 		: in  STD_LOGIC;
		 	last_bit 	: out STD_LOGIC := '0'
	     );
end component;

component	shift_register
	port(							   
			clock 		: in STD_LOGIC;
		 	data_in 	: in STD_LOGIC;
		 	d			: inout STD_LOGIC_VECTOR(7 downto 0) 
	     );
end component;

component	output_register
	port(
			clock			: in  STD_LOGIC;			
			last_bit 		: in  STD_LOGIC;
		 	data_in			: in  STD_LOGIC_VECTOR(7 downto 0);
		 	data_out 		: out STD_LOGIC_VECTOR(7 downto 0);
			data_out_valid 	: out  STD_LOGIC 
		 );
end component;	

-- Signals to manage interconecctions
signal to_output	: STD_LOGIC_VECTOR(7 downto 0);
signal validity		: STD_LOGIC;

begin
	
	-- Mapping for the shift_register, it takes from the inputs data_in and associate the output to the
	-- the signal to_output
	shift_register_map 	: shift_register port map(clock, data_in, to_output);
	
	-- Mapping for the counter, it takes from the inputs first_bit and associate the output to the
	-- the signal validity
	counter_map			: counter port map(clock, first_bit, validity);	
	
	-- Mapping for the output_register it takes the signals validity and to_output as inputs and provides
	--  the output data_out and data_out_valid
	output_register_map	: output_register port map(clock, validity, to_output, data_out, data_out_valid); 
	
	

end sipo;