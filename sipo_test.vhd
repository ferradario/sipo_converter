--------------------------------------------------------------
-- File name : sipo_test.vhdl
-- Purpose   : Generates stimuli
--           :
-- Library   : IEEE
-- Author(s) : Dario Ferrarotti
--------------------------------------------------------------

LIBRARY IEEE;

USE IEEE.std_logic_1164.all;

ENTITY sipo_tb IS

END sipo_tb;

ARCHITECTURE sipo_test OF sipo_tb IS

   	COMPONENT sipo
	 	port(
				 clock 			: in  STD_LOGIC;
			 	 first_bit 		: in  STD_LOGIC;
				 data_in 		: in  STD_LOGIC;
				 data_out 		: out STD_LOGIC_VECTOR (7 downto 0);
				 data_out_valid : out STD_LOGIC
	         );
   	END COMPONENT;

   ----------------------------------------------------------------------------
   CONSTANT N       :  INTEGER  := 7;       	-- Bus Width
   CONSTANT MckPer  :  TIME     := 200 ns;  	-- Master Clk period
   CONSTANT TestLen :  INTEGER  := 200;      	-- No. of Count (MckPer/2) for test


-- I N P U T     S I G N A L S

   	SIGNAL   clk  		: std_logic := '0';
   	SIGNAL   first_bit	: std_logic := '0';
	SIGNAL	 data_in    : STD_LOGIC;
   	
-- O U T P U T     S I G N A L S

	SIGNAL	data_out_valid	: STD_LOGIC;
	SIGNAL	data_out		: STD_LOGIC_VECTOR(7 downto 0);
	SIGNAL	clk_cycle		: INTEGER;
   	SIGNAL	Testing			: Boolean := True;

BEGIN

   sipo_map  : sipo 	PORT MAP(clk, first_bit, data_in, data_out, data_out_valid); 
  
   ----------------------------------------------------------------------------

   -- Generates clk

   clk <= NOT clk AFTER MckPer/2 WHEN Testing ELSE '0';

   -- Runs simulation for TestLen cycles;

   Test_Proc: PROCESS(clk)
   VARIABLE count: INTEGER:= 0;
   
   BEGIN
     clk_cycle <= (count+1)/2;
	 
     CASE count IS
		 	-- Input serial seqiemce of bits to represent 0x65
		 	WHEN  19 	=> first_bit <= '1';
			 			   data_in	<= '1';			 
		   	WHEN  21	=> first_bit <= '0';
			   		       data_in <= '0';
			WHEN  23	=> data_in <= '1';			   
			WHEN  25	=> data_in <= '0';
			WHEN  27	=> data_in <= '0';
			WHEN  29	=> data_in <= '1';
			WHEN  31	=> data_in <= '1';
			WHEN  33	=> data_in <= '0';
			
			-- -- Input serial seqiemce of bits to represent 0x17
			WHEN  35  	=> first_bit <= '1';
						   data_in <= '1';
			WHEN  37	=> first_bit <= '0';
					       data_in <= '1';
			WHEN  39	=> data_in <= '1';
			WHEN  41	=> data_in <= '0';
			WHEN  43	=> data_in <= '1';
			WHEN  45	=> data_in <= '0';
			WHEN  47	=> data_in <= '0';
			WHEN  49	=> data_in <= '0';
			
			-- Input serial seqiemce of bits to represent 0x07
			WHEN  51  	=> first_bit <= '1';
						   data_in <= '1';
			WHEN  53	=> first_bit <= '0';
					       data_in <= '1';
			WHEN  55	=> data_in <= '1';
			WHEN  57	=> data_in <= '0';
			WHEN  59	=> data_in <= '0';
			WHEN  61	=> data_in <= '0';
			WHEN  63	=> data_in <= '0';
			WHEN  65	=> data_in <= '0';
			
			
			-- Input serial seqiemce of bits to represent 0x92
			WHEN  67  	=> first_bit <= '1';
						   data_in <= '0';
			WHEN  69	=> first_bit <= '0';
					       data_in <= '1';
			WHEN  71	=> data_in <= '0';
			WHEN  73	=> data_in <= '0';
			WHEN  75	=> data_in <= '1';
			WHEN  77	=> data_in <= '0';
			WHEN  79	=> data_in <= '0';
			WHEN  81	=> data_in <= '1';
			
			
			-- Input serial seqiemce of bits to represent 0x00
			WHEN  83  	=> first_bit <= '1';
						   data_in <= '0';
			WHEN  85	=> first_bit <= '0';
						   
          	WHEN (TestLen - 1) =>   Testing <= False;
          	WHEN OTHERS => NULL;
	 END CASE;

     count:= count + 1;
	 
   END PROCESS Test_Proc;

END sipo_test;